/**
 * Module dependencies.
 */
var util = require('util')
  , OAuth2Strategy = require('passport-oauth').OAuth2Strategy
  , InternalOAuthError = require('passport-oauth').InternalOAuthError;


/**
 * `Strategy` constructor.
 *
 * The Mixi authentication strategy authenticates requests by delegating to
 * Mixi using the OAuth 2.0 protocol.
 *
 * Applications must supply a `verify` callback which accepts an `accessToken`,
 * `refreshToken` and service-specific `profile`, and then calls the `done`
 * callback supplying a `user`, which should be set to `false` if the
 * credentials are not valid.  If an exception occured, `err` should be set.
 *
 * Options:
 *   - `clientID`      your Mixi application's App ID
 *   - `clientSecret`  your Mixi application's App Secret
 *   - `callbackURL`   URL to which Mixi will redirect the user after granting authorization
 *
 * Examples:
 *
 *     passport.use(new MixiStrategy({
 *         clientID: '123-456-789',
 *         clientSecret: 'shhh-its-a-secret'
 *         callbackURL: 'https://www.example.net/auth/mixi/callback'
 *       },
 *       function(accessToken, refreshToken, profile, done) {
 *         User.findOrCreate(..., function (err, user) {
 *           done(err, user);
 *         });
 *       }
 *     ));
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options, verify) {
  options = options || {};
  options.authorizationURL = options.authorizationURL || 'https://mixi.jp/connect_authorize.pl';
  options.tokenURL = options.tokenURL || 'https://secure.mixi-platform.com/2/token';
  this._scopeSeparator = options.scopeSeparator = options.scopeSeparator || ' ';

  OAuth2Strategy.call(this, options, verify);
  this.name = 'mixi';
}

/**
 * Inherit from `OAuth2Strategy`.
 */
util.inherits(Strategy, OAuth2Strategy);

/**
 * Return extra Mixi-specific parameters to be included in the authorization
 * request.
 *
 * Options:
 *  - `display`  Display mode to render dialog, { `pc`, `touch`, `smartphone` }.
 *
 * @param {Object} options
 * @return {Object}
 * @api protected
 */
Strategy.prototype.authorizationParams = function (options) {
  var params = {},
      display = options.display
      scope = options.scope;

  if (display) {
    params['display'] = display;
  }

  if (scope) {
    if (Array.isArray(scope)) {
      scope.push('r_profile');
    } else {
      scope + ' r_profile';
    }
    params.scope = scope;
  } else {
    params.scope = 'r_profile';
  }

  return params;
};

/**
 * Retrieve user profile from mixi.
 *
 * This function constructs a normalized profile, with the following properties:
 *
 *   - `provider`         always set to `mixi`
 *   - `id`               the user's mixi ID
 *   - `username`         the user's mixi username
 *   - `displayName`      the user's full name
 *   - `name.familyName`  the user's last name
 *   - `name.givenName`   the user's first name
 *   - `name.middleName`  the user's middle name
 *   - `gender`           the user's gender: `male` or `female`
 *   - `profileUrl`       the URL of the profile for the user on mixi
 *   - `emails`           the proxied or contact email address granted by the user
 *
 * @param {String} accessToken
 * @param {Function} done
 * @api protected
 */
Strategy.prototype.userProfile = function(accessToken, done) {
  this._oauth2.getProtectedResource('https://api.mixi-platform.com/2/people/@me/@self', accessToken, function (err, body, res) {
    if (err) { return done(new InternalOAuthError('failed to fetch user profile', err)); }

    try {
      var json = JSON.parse(body).entry;

      var profile = { provider: 'mixi' };
      profile.id = json.id;
      profile.displayName = json.displayName;
      profile.name = json.displayName;
      profile.profileUrl = json.profileUrl;
      //profile.emails = [{ value: json.email }];

      profile._raw = body;
      profile._json = json;

      done(null, profile);
    } catch(e) {
      done(e);
    }
  });
}


/**
 * Expose `Strategy`.
 */
module.exports = Strategy;
